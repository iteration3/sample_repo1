# PlantUML code within markdown file

The following content is a sample class diagram

```puml
@startuml Class diagram for Meeting Scheduler
class Customer {
    Name
    Email
    Address
}
class MeetingInitiator {

}
class Participant{
}
note right of Participant
/'[requirement id=REQ0014 quality=QR0002 test=TC0001]'/

 [[https://gitlab.com/iteration3/sample_repo1/blob/dev/treqs/local/qualityreqs/QR_qualityreqs.md QR0002]]
 [[https://gitlab.com/iteration3/sample_repo1/blob/dev/treqs/local/tests/TC_system.py TC0001]]

 [[https://gitlab.com/treqs-chalmers/sample_repo1/blob/dev/treqs/local/qualityreqs/QR_qualityreqs.md QR0002]]
 [[https://gitlab.com/treqs-chalmers/sample_repo1/blob/dev/treqs/local/tests/TC_system.py TC0001]]
end note
class Meeting{
    location
    startTime
    endTime
}
class Room{
    roomNumber
}
User <|--Participant
User <|--MeetingInitiator
Meeting - "1..1" Room
Participant - " *..1" Meeting
MeetingInitiator - "1..*" Participant
@enduml
```

## Another embedded PlantUML code

```puml
@startuml

Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
note right of Alice
/'[requirement id=REQ0012 test=TC0005]'/


 [[https://gitlab.com/iteration3/sample_repo1/blob/dev/treqs/local/tests/TC_system.py TC0005]]


 [[https://gitlab.com/treqs-chalmers/sample_repo1/blob/dev/treqs/local/tests/TC_system.py TC0005]]
end note
@enduml
```

## Yet, another PlantUML code

```puml
@startuml
actor Foo1
boundary Foo2
control Foo3
entity Foo4
database Foo5
collections Foo6
Foo1 -> Foo2 : To boundary
Foo1 -> Foo3 : To control
Foo1 -> Foo4 : To entity
Foo1 -> Foo5 : To database
Foo1 -> Foo6 : To collections
note left of Foo1
/'[requirement id=REQ0013 quality=QR0002 test=TC0003]'/

 [[https://gitlab.com/iteration3/sample_repo1/blob/dev/treqs/local/qualityreqs/QR_qualityreqs.md QR0002]]
 [[https://gitlab.com/iteration3/sample_repo1/blob/dev/treqs/local/tests/TC_manual.md TC0003]]

 [[https://gitlab.com/treqs-chalmers/sample_repo1/blob/dev/treqs/local/qualityreqs/QR_qualityreqs.md QR0002]]
 [[https://gitlab.com/treqs-chalmers/sample_repo1/blob/dev/treqs/local/tests/TC_manual.md TC0003]]
end note
@enduml
```

<!--Generated UML diagram-->

---
<img src="http://www.plantuml.com/plantuml/svg/jPF1QW8n48RlUOh1Yo2usMrxAXKiug67IhNs929nTzmD99ircIs8zTsRZLlHbXOARKwJcVpvCyaNDdGaBDLQmKW9vo2J8hT2myPOU4GaMURmd1QOrGejIyEQKUt8QBImP-37H6WCmLWBgK9qdsKMdME7Lj7kz51Aae9CeppLdho_J6KbIjhxP6a8mSgy832RyoBZtON5RIqjQYm9P3QOZQTndDp0jXPAqcumdSLnV0M4ZWRpaO-JPPSp1ej5GLIvMyvpIKgiezHeBWcj86dAQ-w4hXIkB5OcuMjbrZp3LqxUpN5bKg5uwn0oqzdgR1hf38w-o-LVE3M7Ttm-MhcT8zHHjOEsbULtdGHvFos4ye_Y_hkRNxlzr16M6JGlVid9aQj6tTnQc0HCvx8bpSj2_C7Gp1Xz55aVJMgzzh0Us8lpaDwzzVld69sIdw5a7xptePD4KT89sx8pRLE1NYYrQzaNie_YNkS2tQ4_iFzZx1q0">
<!--Generated UML diagram-->

---
<img src="http://www.plantuml.com/plantuml/svg/fP6nRi8m441tVyLjJEPCgoweL5341u3O8eGSvqeisN7gkrJYx-i4jb9rw6RfthrdsMhDOhCCCIYr2TuHw3TuJ_KIDeEqr8btLdpgu42V0x6eCWDTc0b-G76VEgOViYuLBeDzBBt1hrh_GT_bPKoG_QKLI1-tcoYSLRdOVAPOLi0tgyDkRypY2QGKLiUjCURbD4Eb55HLAzBp4l7Y9TXwxb94BvIdpZEoZNsWSwO-BR0EgSQ6lb2Adp4aPmEEJiRZzinN5ehp_WhtmkdNmBIlNMj3fCp_Z53NmFWCQbrEu-zz0m00">
<!--Generated UML diagram-->

---
<img src="http://www.plantuml.com/plantuml/svg/jP91Ry8m38Nl-HM-SO8K2XoGc93GTWTnGnLoMmELaXWITzB-_P8MfbusQT9si_sIz_afKPiWwAMrHc4bxE6DUQfARbsD_YDrCrMn4yycDRaY9urqmbpLA5XYeDGjuZ5Zg9A6NKY3fKfMC7xfJ6051uQdxr3AU-a16IhpNkc9Gs7H2q_yK5e-tRvsKOw5mD1PWCzzF3qwUhgtZISRpQ6fr_lNNPPDSxYtQ29ilTjdMJO3eI3hmpRMUJ7IIi7nU1MvXPNMbqOCbfEAhMw4F2PMhWFQcw6JfnjFTMcur3MzQucqe0rNQFI3q4rs-zEWdTWQUcvH_0KfBH_qONkow28bsJ-Y5DybwQwFgoiQIpxyTvfVqtvAHAw6zD9g4wlqdJy1">