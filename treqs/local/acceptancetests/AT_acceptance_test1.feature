# [acceptancetest id=AT0001 story=US0005]
Feature: Schedule resource

    # The first example has three steps
    Scenario: Schedule resource
        Given the user has successfully logged in
        When the user clicks the "book resource" button
        Then the user is presented with resource selection menu

    # The second example has four steps
    Scenario: Set alerts
        Given the user has successfully logged in
        When the user clicks "set alarm" button
        And  the user selects alarm interval time
        Then the system sends notification after the interval time